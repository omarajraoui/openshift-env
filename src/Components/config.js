const appConfig = window.appConfig || {};

export const REACT_APP_OAUTH_CLIENT_ID = appConfig.REACT_APP_OAUTH_CLIENT_ID;
export const REACT_APP_OAUTH_REDIRECT_URI = appConfig.REACT_APP_OAUTH_REDIRECT_URI;
export const REACT_APP_ANALYTICS_URL = appConfig.REACT_APP_ANALYTICS_URL;
